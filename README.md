# Steps for installation

* Run the `npm install` command after cloning this repo
* Run the `gulp` command for the live sync
* Run the `gulp build` to generate the dist folder

## Folder use

* All work should be done in the "app" folder
* The "SCSS" folder should be used for editing styles
* The "CSS" folder is the resulting SCSS files
* Include all CSS and JS files on the index.html to preview the changes



Resources

Time/Day stylesheet script

https://css-tricks.com/snippets/javascript/different-stylesheet-pending-the-time-of-day/

Including a header - http://www.apaddedcell.com/how-automatically-include-your-header-navigation-and-footer-every-page

css triangles - https://codepen.io/callmenick/pen/cBkvE
