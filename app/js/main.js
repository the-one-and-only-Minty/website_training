var app = {
	init: function(){
		this.eventClick();
		this.nightToggle();
	},
	eventClick: function(element){
		$("#other").click(function() {
			console.log('Clicked');
			// Toggle navigation
			$(".navigation").toggleClass("mobileNavigation")
		});
	},
	nightToggle: function(element){
		$('#cssChange').on('click', function(){
		    $('#sky').removeClass('day');
		    $('#sky').addClass('night');
		});
	},

}


$("document").ready(function() {

      app.init();

});

// Hamburger JS

var forEach=function(t,o,r){if("[object Object]"===Object.prototype.toString.call(t))for(var c in t)Object.prototype.hasOwnProperty.call(t,c)&&o.call(r,t[c],c,t);else for(var e=0,l=t.length;l>e;e++)o.call(r,t[e],e,t)};

    var hamburgers = document.querySelectorAll(".hamburger");
    if (hamburgers.length > 0) {
      forEach(hamburgers, function(hamburger) {
        hamburger.addEventListener("click", function() {
          this.classList.toggle("is-active");
        }, false);
      });
    }